# Local Development Run

## Installation

### Clone the repository:

```bash

git clone https://gitlab.com/nextlab3405645/nextappvuefrontend.git

Navigate to the project directory:
cd nestlabsevaluation-frontend

```
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

# Deployment Process
## Frontend Deployment (Vue 3)

### Prerequisites

- Backend API URL [link](https://nextlab-app.onrender.com)
- Vue 3 frontend codebase ready for deployment

### Steps

1. **Clone the Repository:**

    ```bash
    git clone https://gitlab.com/nextlab3405645/nextappvuefrontend.git
    cd nestlabsevaluation-frontend
    ```

2. **Frontend Setup:**

    - Ensure that the frontend code is ready for deployment.
    - Update API URLs in the codebase with the deployed backend URL.

3. **Manual Deployment on Render (Static Site):**

    - Log in to your Render account.
    - Create a new static site for the frontend.
    - Deploy the frontend code.

